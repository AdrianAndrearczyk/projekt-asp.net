namespace MVCInBuiltFeatures.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodaniewalidatorówdoksiążekibibliotek : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Books", "Title", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Books", "Author", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Books", "Isbn", c => c.String(nullable: false, maxLength: 13));
            AlterColumn("dbo.Libraries", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Libraries", "City", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Libraries", "Street", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Libraries", "Street", c => c.String());
            AlterColumn("dbo.Libraries", "City", c => c.String());
            AlterColumn("dbo.Libraries", "Name", c => c.String());
            AlterColumn("dbo.Books", "Isbn", c => c.String());
            AlterColumn("dbo.Books", "Author", c => c.String());
            AlterColumn("dbo.Books", "Title", c => c.String());
        }
    }
}
