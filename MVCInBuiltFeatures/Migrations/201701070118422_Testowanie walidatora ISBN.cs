namespace MVCInBuiltFeatures.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestowaniewalidatoraISBN : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Books", "Isbn", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Books", "Isbn", c => c.String(nullable: false, maxLength: 13));
        }
    }
}
