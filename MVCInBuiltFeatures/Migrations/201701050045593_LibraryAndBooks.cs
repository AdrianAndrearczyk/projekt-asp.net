namespace MVCInBuiltFeatures.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LibraryAndBooks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        BookID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Author = c.String(),
                        Isbn = c.String(),
                        LibraryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BookID)
                .ForeignKey("dbo.Libraries", t => t.LibraryID, cascadeDelete: true)
                .Index(t => t.LibraryID);
            
            CreateTable(
                "dbo.Libraries",
                c => new
                    {
                        LibraryID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        City = c.String(),
                        Street = c.String(),
                    })
                .PrimaryKey(t => t.LibraryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "LibraryID", "dbo.Libraries");
            DropIndex("dbo.Books", new[] { "LibraryID" });
            DropTable("dbo.Libraries");
            DropTable("dbo.Books");
        }
    }
}
