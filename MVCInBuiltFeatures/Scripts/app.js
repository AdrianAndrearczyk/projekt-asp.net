﻿var ViewModel = function () {
    var self = this;
    self.books = ko.observableArray();
    self.error = ko.observable();
    self.detail = ko.observable();
    self.libraries = ko.observableArray();
    self.newBook = {
        Library: ko.observable(),
        Title: ko.observable(),
        Author: ko.observable(),
        Isbn: ko.observable()
    }

    var booksUri = '/api/book/';
    var librariesUri = '/api/library/';

    function ajaxHelper(uri, method, data) {
        self.error(''); 
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    function getAllBooks() {
        ajaxHelper(booksUri, 'GET').done(function (data) {
            self.books(data);
        });
    }

    self.getBookDetail = function (item) {
        ajaxHelper(booksUri + item.BookID, 'GET').done(function (data) {
            self.detail(data);
        });
    }

    function getLibraries() {
        ajaxHelper(librariesUri, 'GET').done(function (data) {
            self.libraries(data);
        });
    }


    self.addBook = function (formElement) {
        var book = {
            LibraryID: self.newBook.Library().LibraryID,
            Title: self.newBook.Title(),
            Author: self.newBook.Author(),
            Isbn: self.newBook.Isbn()
        };

        ajaxHelper(booksUri, 'POST', book).done(function (item) {
            self.books.push(item);
        });
    }

    // Fetch the initial data.
    getAllBooks();
    getLibraries();
};

ko.applyBindings(new ViewModel());