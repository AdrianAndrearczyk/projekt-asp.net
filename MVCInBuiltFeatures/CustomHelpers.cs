﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MVCInBuiltFeatures
{
    public static class CustomHelpers
    {
        public static IHtmlString Image(this HtmlHelper helper, string src, string alt)
        {
            TagBuilder tb = new TagBuilder("img");
            tb.Attributes.Add("src", VirtualPathUtility.ToAbsolute(src));
            tb.Attributes.Add("alt", alt);
            return new MvcHtmlString(tb.ToString(TagRenderMode.SelfClosing));
        }

        public static HtmlString CarouselItem(this HtmlHelper htmlHelper, string className, string secondClassName, string src, string alt, string caption, string author)
        {

            var imgb = new TagBuilder("img");
            imgb.Attributes.Add("src", VirtualPathUtility.ToAbsolute(src));
            imgb.Attributes.Add("alt", alt);

            var ssb = new StringBuilder();
            ssb.AppendFormat("<h3><em>" + caption + "</em></h3><p>"+ author +"</p>");

            var sdb = new TagBuilder("div");
            sdb.Attributes.Add("class", secondClassName);
            sdb.InnerHtml += ssb.ToString();

            var fdb = new TagBuilder("div");
            fdb.Attributes.Add("class", className);
            fdb.InnerHtml += imgb.ToString(TagRenderMode.SelfClosing);
            fdb.InnerHtml += sdb.ToString();

            return new HtmlString(fdb.ToString());
        }
    }
}