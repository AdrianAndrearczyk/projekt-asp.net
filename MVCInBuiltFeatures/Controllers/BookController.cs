﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MVCInBuiltFeatures.Models;
using System.Threading.Tasks;

namespace MVCInBuiltFeatures.Controllers
{
    public class BookController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/book
        public IQueryable<BookDTO> GetBooks()
        {
            var books = from b in db.Books
                        select new BookDTO()
                        {
                            BookID = b.BookID,
                            Title = b.Title,
                            LibraryName = b.Library.Name        
                        };

            return books;
        }

        // GET api/Book/5
        [ResponseType(typeof(BookDetailDTO))]
        public async Task<IHttpActionResult> GetBook(int id)
        {
            var book = await db.Books.Include(b => b.Library).Select(b =>
                new BookDetailDTO()
                {
                    BookID = b.BookID,
                    Title = b.Title,
                    Isbn = b.Isbn,
                    Author = b.Author,
                    LibraryName = b.Library.Name
                }).SingleOrDefaultAsync(b => b.BookID == id);
            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        // PUT: api/Book/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBook(int id, Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != book.BookID)
            {
                return BadRequest();
            }

            db.Entry(book).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Book
        [ResponseType(typeof(Book))]
        public async Task<IHttpActionResult> PostBook(Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (book.Isbn.Contains("-")) book.Isbn = book.Isbn.Replace("-", "");
            db.Books.Add(book);
            await db.SaveChangesAsync();

            // Load author name
            db.Entry(book).Reference(x => x.Library).Load();

            var dto = new BookDTO()
            {
                BookID = book.BookID,
                Title = book.Title,
                LibraryName = book.Library.Name
            };

            return CreatedAtRoute("DefaultApi", new { id = book.BookID }, dto);
        }


        // DELETE: api/Book/5
        [ResponseType(typeof(Book))]
        public async Task<IHttpActionResult> DeleteBook(int id)
        {
            Book book = await db.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }

            db.Books.Remove(book);
            await db.SaveChangesAsync();

            return Ok(book);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BookExists(int id)
        {
            return db.Books.Count(e => e.BookID == id) > 0;
        }
    }
}