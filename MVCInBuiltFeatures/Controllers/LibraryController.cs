﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MVCInBuiltFeatures.Models;
using System.Threading.Tasks;

namespace MVCInBuiltFeatures.Controllers
{
    public class LibraryController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Library
        public IQueryable<Library> GetLibraries()
        {
            return db.Libraries;
        }

        // GET: api/Library/5
        [ResponseType(typeof(Library))]
        public async Task<IHttpActionResult> GetLibrary(int id)
        {
            Library library = await db.Libraries.FindAsync(id);
            if (library == null)
            {
                return NotFound();
            }

            return Ok(library);
        }

        // PUT: api/Library/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLibrary(int id, Library library)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != library.LibraryID)
            {
                return BadRequest();
            }

            db.Entry(library).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LibraryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Library
        [ResponseType(typeof(Library))]
        public async Task<IHttpActionResult> PostLibrary(Library library)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Libraries.Add(library);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = library.LibraryID }, library);
        }

        // DELETE: api/Library/5
        [ResponseType(typeof(Library))]
        public async Task<IHttpActionResult> DeleteLibrary(int id)
        {
            Library library = await db.Libraries.FindAsync(id);
            if (library == null)
            {
                return NotFound();
            }

            db.Libraries.Remove(library);
            await db.SaveChangesAsync();

            return Ok(library);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LibraryExists(int id)
        {
            return db.Libraries.Count(e => e.LibraryID == id) > 0;
        }
    }
}