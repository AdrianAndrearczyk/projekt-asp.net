﻿using MVCInBuiltFeatures.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCInBuiltFeatures.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var lastFiveProducts = (from b in db.Books
                                    orderby b.BookID descending
                                    select b).Take(5);
            ViewBag.Books = db.Books.Count();
            return View(lastFiveProducts.ToList());
        }

        public ActionResult About()
        {
            IQueryable<BooksViewModels> data =
                from book in db.Books
                group book by book.Library.Name into booksGroup
                select new BooksViewModels()
                {
                    Name = booksGroup.Key,
                    BookCount = booksGroup.Count(),
                };
            ViewBag.Books = db.Books.Count();
            ViewBag.Users = db.Users.Count();
            return View(data.ToList());
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Strona kontaktowa";

            return View();
        }
        public ActionResult API()
        {
            return View();
        }
    }
}