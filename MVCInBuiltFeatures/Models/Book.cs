﻿using MVCInBuiltFeatures.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCInBuiltFeatures.Models
{
    public class Book
    {
        public int BookID { get; set; }

        [Required]
        [Display(Name = "Tytuł")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} litery.", MinimumLength = 2)]

        public string Title { get; set; }

        [Required]
        [Display(Name = "Autor")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Author { get; set; }

        [Required]
        [Display(Name = "ISBN")]
        [IsbnValidator]
        public string Isbn { get; set; }

        public int LibraryID { get; set; }

        public Library Library { get; set; }

    }
}