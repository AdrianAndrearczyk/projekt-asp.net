﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace MVCInBuiltFeatures.Models
{
    public class BooksViewModels
    { 
        public string Name { get; set; }
        public int BookCount { get; set; }
        public int Books { get; set; }
    }
}