﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCInBuiltFeatures.Models
{
    public class BookDTO
    {
        public int BookID { get; set; }
        public string Title { get; set; }
        public string LibraryName { get; set; }
    }
}