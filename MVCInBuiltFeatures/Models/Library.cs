﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCInBuiltFeatures.Models
{
    public class Library
    {

        public int LibraryID { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        [Display(Name = "Nazwa biblioteki")]
        public string Name { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        [Display(Name = "Miejscowość")]
        public string City { get; set; }
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        [Display(Name = "Ulica")]
        public string Street { get; set; }
    }
}